<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $query = Product::query();

        if ($request->has('search')) {
            $query->where('name', 'like', '%' . $request->input('search') . '%')
                ->orWhere('description', 'like', '%' . $request->input('search') . '%');
        }

        $products = $query->paginate(10);

        return response()->json($products);
    }

    public function store(ProductRequest $request)
    {
        // $product = Product::create($request->validated());
        $product = new Product;
        $product->name = $request->name;
        $product->description = $request->description;
        $product->stock = $request->stock;
        $product->unit = $request->unit;
        $product->is_publish = $request->is_publish;
        $product->save();

        return response()->json($product, 201);
    }

    public function show(Product $product)
    {
        return response()->json($product);
    }

    public function update(Request $request, Product $product)
    {
        // $product->update($request->validated());
        $product->name = $request->name;
        $product->description = $request->description;
        $product->stock = $request->stock;
        $product->unit = $request->unit;
        $product->is_publish = $request->is_publish;
        $product->save();

        return response()->json($product);
    }

    public function destroy(Product $product)
    {
        $product->delete();

        return response()->json(null, 204);
    }
}
