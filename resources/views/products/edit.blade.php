@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Edit Product</h1>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{ route('products.update', $product->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name"
                            value="{{ old('name', $product->name) }}">
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control" id="description" name="description">{{ old('description', $product->description) }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="stock">Stock</label>
                        <input type="number" class="form-control" id="stock" name="stock"
                            value="{{ old('stock', $product->stock) }}">
                    </div>
                    <div class="form-group">
                        <label for="unit">Unit</label>
                        <select class="form-control" id="unit" name="unit">
                            <option value="pcs" {{ $product->unit == 'pcs' ? 'selected' : '' }}>pcs</option>
                            <option value="kg" {{ $product->unit == 'kg' ? 'selected' : '' }}>kg</option>
                            <option value="liter" {{ $product->unit == 'liter' ? 'selected' : '' }}>liter</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="is_publish">Is Publish</label>
                        <select class="form-control" id="is_publish" name="is_publish">
                            <option value="1" {{ $product->is_publish ? 'selected' : '' }}>Yes</option>
                            <option value="0" {{ !$product->is_publish ? 'selected' : '' }}>No</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Update</button>
                </form>
            </div>
        </div>
    </div>
@endsection
